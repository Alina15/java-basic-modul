package fifthweek;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int res = 1;
        for (int i = 1; i <= n; i++) {
            res = res * i;
        }
        System.out.println("Факториал в цикле: " + res);

        int factRec = factorial(n);
        System.out.println("Факториал в рекурсии: " + factRec);

        int factTailRec = factorialTail(n, 1);
        System.out.println("Факториал в хвостовой рекурсии : " + factTailRec);
    }

    public static int factorial(int input) {
        if (input <= 1) {
            return 1;
        }
        return input * factorial(input - 1);
    }

    public static int factorialTail(int input, int result) {
        if (input <= 1) {
            return result;
        }
        else {
            return factorialTail(input - 1, input * result);
        }
    }
}

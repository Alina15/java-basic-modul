package fifthweek;
/*
Развернуть строку рекурсивно.

abcde -> edcba
 */
import java.util.Scanner;
public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();

        System.out.println(reverseString(s));
    }

    public static String reverseString(String input) {
        String right;
        String left;
        int length = input.length();

        if (length <= 1) {
            return input;
        }

        left = input.substring(0, length / 2);
        right = input.substring(length / 2, length);

        return reverseString(right) + reverseString(left);
    }

}

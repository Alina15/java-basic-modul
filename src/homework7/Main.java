package homework7;


public class Main {
    public static void main(String[] args) {
        Library library = new Library();
        Book book1 = new Book("Война и мир", "Лев Толстой");
        Book book2 = new Book("Преступление и наказание", "Федор Достоевский");
        Book book3 = new Book("Война и мир", "Лев Толстой");
        Book book4 = new Book("Мастер и Маргарита", "Михаил Булгаков");
        Book book5 = new Book("Собачье сердце", "Михаил Булгаков");

        Visitor visitor1 = new Visitor("Иванов Иван");
        Visitor visitor2 = new Visitor("Петров Пётр");
        Visitor visitor3 = new Visitor("Сергеев Сергей");

        library.addBook(book1);
        library.addBook(book2);
        library.addBook(book3);
        library.addBook(book4);
        library.addBook(book5);
        System.out.println("---------------------");

        library.deliteBook(book3);
        library.displayBooks();
        System.out.println("---------------------");

        System.out.println("Вывод книги по названию:");
        library.readBookOfName("Преступление и наказание");
        library.readBookOfName("Аист марабу");
        System.out.println("---------------------");

        System.out.println("Вывод списка книг по автору:");
        library.readBookOfWriter("Михаил Булгаков");
        library.readBookOfWriter("Чак Паланик");
        System.out.println("---------------------");

        library.giveBook(visitor1,book2);
        library.giveBook(visitor2,book2);
        library.giveBook(visitor2,book4);
        library.giveBook(visitor3,book1);
        library.giveBook(visitor3,book5);
        library.displayVisitor();
        System.out.println("---------------------");

        library.returnBook(visitor1, book2, 4);
        library.giveBook(visitor1,book2);
        library.returnBook(visitor1, book2, 5);
        library.getScores(book2);




    }
}

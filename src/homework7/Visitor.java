package homework7;

public class Visitor {
    private final String visitorName;
    private int book;
    private String nameBook;
    private int id;
    public Visitor(String visitorName){
        this.visitorName = visitorName;
        id = 0;
        book = -1;
        nameBook = "";
    }

    public String getVisitorName() {
        return visitorName;
    }

    public int getId() {
        return id;
    }

    public int getBook() {
        return book;
    }

    public void setBook(int book) {
        this.book = book;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameBook() {
        return nameBook;
    }

    public void setNameBook(String nameBook) {
        this.nameBook = nameBook;
    }
}

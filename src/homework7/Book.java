package homework7;

import java.util.Arrays;

public class Book {
    private final String nameBook;
    private final String nameWriter;
    private boolean availableBook;
    private double[] score;
    private int number;
    private int capacity;
    public Book(String nameBook, String nameWriter){
        this.nameBook = nameBook;
        this.nameWriter = nameWriter;
        availableBook = true;
        this.score = new double[2];
        capacity = 2;
        number = 0;
    }


    public String getNameBook() {
        return nameBook;
    }

    public String getNameWriter() {
        return nameWriter;
    }
    public boolean getAvailableBook(){
        return availableBook;
    }

    public void setAvailableBook(boolean availableBook) {
        this.availableBook = availableBook;
    }

    public double[] getScore() {
        return score;
    }


    public void addScore(double scores){
        if (number >= capacity) {
            capacity = 2 * capacity;
            score = Arrays.copyOf(score, capacity);
        }
        this.score[number++] = scores;
    }
    public int getNumber() {
        return number;
    }

}

package homework7;

import java.util.Arrays;

public class Library {
    private Book[] books;
    private Visitor[] visitors;
    private int sizeVisitor;
    private int capacity;
    private int capacityVisitor;
    private int bookCount;
    private int countId;


    public Library(){
        int DEFAULT_CAPACITY = 2;
        this.books = new Book[DEFAULT_CAPACITY];
        capacity = DEFAULT_CAPACITY;
        capacityVisitor = DEFAULT_CAPACITY;
        sizeVisitor = 0;
        bookCount = 0;
        countId = 1;
        this.visitors = new Visitor[DEFAULT_CAPACITY];
    }
    public void addBook(Book book) {
            if (books[0] == null){
                books[0] = book;
                bookCount++;
                System.out.println("Книга \"" + book.getNameBook() + "\" добавлена в библиотеку.");
            }else
                if (isBookExists(book.getNameBook()) == -1) {

                if (bookCount >= capacity) {
                    capacity = 2 * capacity;
                    books = Arrays.copyOf(books, capacity);
                }
                books[bookCount] = book;
                bookCount++;
                System.out.println("Книга \"" + book.getNameBook() + "\" добавлена в библиотеку.");
            } else {
                System.out.println("Книга \"" + book.getNameBook() + "\" уже существует.");
            }
        }
//    }
public void giveBook(Visitor visitor, Book book) {
        if(visitor.getBook()==-1){
           if(isBookExists(book.getNameBook()) != -1) {
               if (books[isBookExists(book.getNameBook())].getAvailableBook()){
                    addVisitor(visitor,isBookExists(book.getNameBook()), book.getNameBook());
               }else System.out.println(book.getNameBook() + ": книга на руках");
           }else
               System.out.println(book.getNameBook() + ": такой книги нет");
        }else
            System.out.println("У посетителя \"" + visitor.getVisitorName() + "\" есть книга");

}
private void addVisitor(Visitor visitor, int indexBook, String nameBook) {
    if (visitors[0] == null) {
        visitors[0] = visitor;
        books[indexBook].setAvailableBook(false);
        visitors[0].setBook(indexBook);
        if(visitors[0].getId() == 0){
            visitors[0].setId(countId);
            visitors[0].setNameBook(nameBook);
            countId++;
        }
        sizeVisitor++;
        System.out.println("Посетитель \"" + visitor.getVisitorName() + "\" взял книгу ");
    } else {
                if (sizeVisitor >= capacityVisitor) {
                    capacityVisitor = 2 * capacityVisitor;
                    visitors = Arrays.copyOf(visitors, capacityVisitor);
                }
                books[indexBook].setAvailableBook(false);
                visitors[sizeVisitor] = visitor;
                visitors[sizeVisitor].setBook(indexBook);
                visitors[sizeVisitor].setNameBook(nameBook);
                if(visitors[sizeVisitor].getId() == 0){
                    visitors[sizeVisitor].setId(countId);
                    countId++;
                }
                sizeVisitor++;
                System.out.println("Посетитель \"" + visitor.getVisitorName() + "\" взял книгу");

        }
    }
//}
    public void returnBook(Visitor visitor, Book book, int score){

if (isBookExists(book.getNameBook()) == visitor.getBook()){
    visitor.setBook(-1);
    book.setAvailableBook(true);
    book.addScore(score);
    System.out.println("Посетитель \"" + visitor.getVisitorName() + "\" сдал книгу");
}else System.out.println("Посетитель \"" + visitor.getVisitorName() + "\" брал не эту книгу");

    }
    public void deliteBook(Book book) {
        if (isBookExists(book.getNameBook())!=-1 && book.getAvailableBook()) {
            if (isBookExists(book.getNameBook()) >= 0 && isBookExists(book.getNameBook()) < bookCount) {
                for (int i = isBookExists(book.getNameBook()); i < bookCount - 1; i++) {
                    books[i] = books[i + 1];
                }
                bookCount--;
            }
            System.out.println("Книга \"" + book.getNameBook() + "\" удалена.");
        }
    }


    private int isBookExists(String name) {
        for (int i = 0; i < bookCount; i++) {
            if (books[i].getNameBook().equals(name)) {
                return i;
            }
        }
        return -1;
    }
    public void readBookOfName(String name){
        int j = 0;
        for (int i = 0; i < bookCount; i++) {
            if (books[i].getNameBook().equals(name)) {
                System.out.println("Название: " + books[i].getNameBook() + ", Автор: " + books[i].getNameWriter());
                j++;
                break;
            }
        }
        if (j == 0){
            System.out.println(name + ": такой книги нет");
        }

    }
    public void readBookOfWriter(String name){
        int j = 0;
        for (int i = 0; i < bookCount; i++) {
            if (books[i].getNameWriter().equals(name)) {
                System.out.println("Название: " + books[i].getNameBook() + ", Автор: " + books[i].getNameWriter());
                j++;
            }
        }
        if (j == 0){
            System.out.println(name + ": такого автора нет");
        }


    }
    public void getScores(Book book){
        double sum = 0;
        if(book.getNumber()==0){
            System.out.println("У данной книги нет оценок");
        }else {
            for (int i = 0; i < book.getNumber(); i++) {
                sum += book.getScore()[i];
            }
            System.out.println("Средняя оценка за книгу \"" + book.getNameBook()+"\": " + sum/book.getNumber());
        }
    }
    public void displayBooks() {
        System.out.println("Список книг в библиотеке:");
        for (int i = 0; i < bookCount; i++) {
            Book book = books[i];
            System.out.println("Название: " + book.getNameBook() + ", Автор: " + book.getNameWriter());
        }
    }
    public void displayVisitor() {
        System.out.println("Список людей в библиотеке:");

        for (int i = 0; i < sizeVisitor; i++) {
            System.out.println("Имя: " + visitors[i].getVisitorName() + ", ID: " + visitors[i].getId());
        }
    }
}

package homework8.Task4;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Top3 {
    public Top3(){
    }
    public List<Participant> getTop3(List<Dog> dogs){
        List<Participant> participants = new ArrayList<>();

        for (Dog dog : dogs) {
            participants.add(new Participant(dog.getOwnerName(), dog.getDogName(), dog.getAverageScore()));
        }

        participants.sort(Comparator.comparingDouble(Participant::getAverageScore).reversed());

        return participants.subList(0, 3);

    }
}

package homework8.Task4;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Top3 top3 = new Top3();
        List<Dog> dogs = new ArrayList<>();
        dogs.add(new Dog("Иван", "Жучка", List.of(7.0, 6.0, 7.0)));
        dogs.add(new Dog("Николай", "Кнопка", List.of(8.0, 8.0, 7.0)));
        dogs.add(new Dog("Анна", "Цезарь", List.of(4.0, 5.0, 6.0)));
        dogs.add(new Dog("Дарья", "Добряш", List.of(9.0, 9.0, 9.0)));

        List<Participant> winners = top3.getTop3(dogs);

        for (Participant winner : winners) {
            System.out.println(winner);
        }

    }
}

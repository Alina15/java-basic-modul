package homework8.Task4;


public class Participant {
    private final String ownerName;
    private final String dogName;
    private final double averageScore;

    public Participant(String ownerName, String dogName, double averageScore) {
        this.ownerName = ownerName;
        this.dogName = dogName;
        this.averageScore = averageScore;
    }

    public String toString() {
        return ownerName + ": " + dogName + ", средняя оценка " + averageScore;
    }


    public double getAverageScore() {
        return averageScore;

    }
}

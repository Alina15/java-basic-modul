package homework8.Task4;

import java.util.List;

public class Dog {
    private final String ownerName;
    private final String dogName;
    private final List<Double> scores;
    public Dog(String ownerName, String dogName, List<Double> scores) {
        this.ownerName = ownerName;
        this.dogName = dogName;
        this.scores = scores;
    }
    public double getAverageScore() {
        return Math.floor((scores.get(0) + scores.get(1) + scores.get(2))/3.0*10)/10.0;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public String getDogName() {
        return dogName;
    }

}

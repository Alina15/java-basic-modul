package homework8.Task1;

public abstract class Mammal extends Behaviour implements WayOfBirth{
    @Override
    public void giveBirth() {
        System.out.println("Животное живородящее");
    }
}

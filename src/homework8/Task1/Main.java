package homework8.Task1;

public class Main {
    public static void main(String[] args) {
        Bat bat = new Bat();
        bat.eat();
        bat.sleep();
        bat.giveBirth();
        bat.movement();
        System.out.println("---------------------");
        Dolphin dolphin = new Dolphin();
        dolphin.eat();
        dolphin.sleep();
        dolphin.giveBirth();
        dolphin.movement();
        System.out.println("---------------------");
        Eagle eagle = new Eagle();
        eagle.eat();
        eagle.sleep();
        eagle.giveBirth();
        eagle.movement();
        System.out.println("---------------------");
        GoldFish goldFish = new GoldFish();
        goldFish.eat();
        goldFish.sleep();
        goldFish.giveBirth();
        goldFish.movement();
    }
}

package homework8.Task1;

public abstract class Bird extends Behaviour implements WayOfBirth{
    @Override
    public void giveBirth() {
        System.out.println("Животное откладывает яйца");
    }
}

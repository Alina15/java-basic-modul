package homework8.Task3;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        int n = 3;
        int m = 4;

        ArrayList<ArrayList<Integer>> matrix = new ArrayList<>();

        for (int i = 0; i < m; i++) {
            ArrayList<Integer> row = new ArrayList<>();
            for (int j = 0; j < n; j++) {
                row.add(i + j);
            }
            matrix.add(row);
        }

        for (ArrayList<Integer> row : matrix) {
            for (Integer element : row) {
                System.out.print(element + " ");
            }
            System.out.println();
        }
    }
}

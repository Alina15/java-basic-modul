package homework8.Task2;

public class Main {
    public static void main(String[] args) {
        BestCarpenterEver carpenter = new BestCarpenterEver();


        Stool stool = new Stool();
        Table table = new Table();

        System.out.println(carpenter.canRepair(stool));
        System.out.println(carpenter.canRepair(table));
    }
}

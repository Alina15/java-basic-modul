package dz3;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int r = n;
        int k = 0;
        while (n > 0){
            k *=10;
            k = k + (n%10);
            n=n/10;
        }
        if (String.valueOf (r).length ()!=String.valueOf (k).length ()){
            while (k > 0){
                int i = k%10;
                k/=10;
                System.out.println(i);
            }
            System.out.println(0);
        }else {
            while (k > 0) {
                int i = k % 10;
                k /= 10;
                System.out.println(i);
            }
        }
    }
}

package dz3;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        int res = 0;
        for (int i = n; i <= m; i++){
            res += i;
        }
        System.out.println(res);
    }
}

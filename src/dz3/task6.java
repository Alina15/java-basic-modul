package dz3;

import java.util.Scanner;

public class task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int a=0;
        int b=0;
        int c=0;
        int d=0;
        while (m > 0) {
            if (m-8>=0){
                a++;
                m-=8;
            }else  if (m-4>=0){
                b++;
                m-=4;
            }else if (m-2>=0){
                c++;
                m-=2;
            }else{
                d++;
                m-=1;
            }
        }
        System.out.println(a +" "+ b +" "+ c+ " "+ d);
    }
}

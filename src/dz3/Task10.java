package dz3;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int height= scanner.nextInt();
        int a=0;
        for (int i = 0; i < height; i++) {

            for (int j = 0; j < height - i - 1; j++) {
                System.out.print(' ');
            }
            for (int k = 0; k < 2 * i + 1; k++) {
                System.out.print('#');
                if (i+1 == height)
                    a++;
            }
            System.out.println();
        }
        for(int i=0; i < a/2;i++ ){
            System.out.print(' ');
        }
        System.out.println('|');
    }
}

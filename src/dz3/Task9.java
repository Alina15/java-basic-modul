package dz3;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int b=0;
        while (true) {
            int n = scanner.nextInt();
            if(n>0){
                break;
            }else
                b++;
        }
        System.out.println(b);
    }
}

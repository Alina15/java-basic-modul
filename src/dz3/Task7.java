package dz3;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        int symbolCount=0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != ' ') {
                symbolCount++;
            }
        }
        System.out.println(symbolCount);
    }
}

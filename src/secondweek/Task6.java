package secondweek;

import java.util.Scanner;

/*
Реализовать System.out.println(), используя System.out.print() и перенос \n
Входные данные: два слова, считываемые из консоли

Входные данные
Hello World
Выходные данные
Hello
World
 */
public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String first = scanner.next();
        String second = scanner.next();

        System.out.print(first + "\n" + second);
//        System.out.println(first);
//        System.out.print(second);
    }
}

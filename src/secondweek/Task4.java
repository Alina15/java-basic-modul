package secondweek;

import java.util.Scanner;

/*
 Считать данные из консоли о типе номера отеля.
 1 - VIP, 2 - Premium, 3 - Standard
 Вывести цену номера VIP = 125, Premium = 110, Standard = 100
 */
public class Task4 {
    final static int VIP_PRICE = 125;
    final static int PREMIUM_PRICE = 110;
    final static int STANDARD_PRICE = 100;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int roomType = scanner.nextInt();
        //Стандартное решение через if-else
        if (roomType == 1) {
            System.out.println("VIP Price >> " + VIP_PRICE);
        } else if (roomType == 2) {
            System.out.println("Premium Price >> " + PREMIUM_PRICE);
        } else if (roomType == 3) {
            System.out.println("Standard Price >> " + STANDARD_PRICE);
        } else {
            System.out.println("введите корректный номер");
        }

        //switch (case)
        switch (roomType) {
            case 1:
                System.out.println("VIP Price >> " + VIP_PRICE);
                break;
            case 2:
                System.out.println("Premium Price >> " + PREMIUM_PRICE);
                break;
            case 3:
                System.out.println("Standard Price >> " + STANDARD_PRICE);
                break;
            default:
                System.out.println("введите корректный номер");
                break;
        }
        //новая (современная) версия switch-case через лямбда
        switch (roomType) {
            case 1 -> System.out.println("VIP Price >> " + VIP_PRICE);
            case 2 -> System.out.println("Premium Price >> " + PREMIUM_PRICE);
            case 3 -> System.out.println("Standard Price >> " + STANDARD_PRICE);
            default -> System.out.println("введите корректный номер");
        }
    }
}

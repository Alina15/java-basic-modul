package dz4;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double[] arr = new double[n];
        double t=0;

        //Заполняем массив пользовательскими данными
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextDouble();
        }

        for (int i = 0; i < n; i++) {
            t+=arr[i];
        }
        System.out.println(t/n);
    }
}

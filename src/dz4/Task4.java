package dz4;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n];

        //Заполняем массив пользовательскими данными
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
        int a = 1;

        for (int i = 0; i < n - 1; i++) {
            if (arr[i] == arr[i + 1]) {
                a++;
            } else {
                System.out.println(a + " " + arr[i]);
                a = 1;
            }
        }
        if (arr[n-1] == arr[n - 2]){
            System.out.println(2 + " " + arr[n-1]);
        }else {
            System.out.println(1 + " " + arr[n-1]);
        }



    }
}

package dz4;

import java.util.Arrays;
import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n];
        int[] arr1 = new int[n];

        //Заполняем массив пользовательскими данными
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
            arr1[i] = arr[i] * arr[i];
        }
        Arrays.sort(arr1);
        for (int i = 0; i < n; i++) {
            System.out.print(arr1[i] + " ");
        }


    }
}

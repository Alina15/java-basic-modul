package dz4;

import java.util.Arrays;
import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n];

        //Заполняем массив пользовательскими данными
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
        int m = scanner.nextInt();
        int a = Math.abs(m-arr[0]);
        int b = 0;
        for (int i = 0; i < n; i++) {
            if (Math.abs(m-arr[i]) < a){
                a = Math.abs(m-arr[i]);
                b = arr[i];
            } else if (Math.abs(m-arr[i]) == a) {
                if (b < arr[i])
                    b = arr[i];
            }
        }
        System.out.print(b);


    }
}

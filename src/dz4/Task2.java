package dz4;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean flag = false;
        int a = scanner.nextInt();
        int[] arr1 = new int[a];
        //Заполняем массив пользовательскими данными
        for (int i = 0; i < a; i++) {
            arr1[i] = scanner.nextInt();
        }
        int b = scanner.nextInt();
        int[] arr2 = new int[b];
        for (int i = 0; i < b; i++) {
            arr2[i] = scanner.nextInt();
        }
        if (a == b) {

            for (int i = 0; i < a; i++) {
                if (arr1[i] != arr2[i]) {
                    flag = false;
                    break;
                } else
                    flag = true;
            }
        }
            System.out.println(flag);


    }
}

package thirdweek;

import java.util.Scanner;

/*
Даны числа m < 13 и n < 7.
Вывести все степени (от 0 до n включительно) числа m с помощью цикла.
3 6
->
1
3
9
27
81
243
729
 */
public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();

        if (m < 13 && n < 7) {
            for (int i = 0; i <= n; ++i) {
                System.out.println(Math.pow(m,i));

            }

        } else {
            System.out.println("Некорректные числа");
        }
    }
    }


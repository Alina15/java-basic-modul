package thirdweek;

import java.util.Scanner;

/*
На вход подается число n и последовательность целых чисел длины n.
Вывести два максимальных числа в этой последовательности без использования массивов.
5
1 3 5 4 5 -> 5 5
 */
public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();


        int firstNumber = scanner.nextInt();
        int secondNumber = scanner.nextInt();
        int thirdNumber;
        for (int i = 0; i < n-2; i++){
            thirdNumber = scanner.nextInt();
            if (firstNumber < thirdNumber ){
                firstNumber = thirdNumber;
            } else if (secondNumber < thirdNumber) {
                secondNumber = thirdNumber;
            }
        }
        System.out.println(firstNumber +" "+  secondNumber);
    }
}


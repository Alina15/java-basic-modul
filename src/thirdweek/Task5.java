package thirdweek;
import java.util.Scanner;
public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int result = 0;
        while (true) {
            if (scanner.nextInt() < 0) {
                result++;
            } else {
                break;
            }
        }
        System.out.println("Количество отрицательных чисел: " + result);

        // 2 example:
        Scanner scanner2 = new Scanner(System.in);
        int count = 0;
        for (int i = scanner2.nextInt(); i < 0; i = scanner2.nextInt()) {
            count++;
        }
        System.out.println("Количество отрицательных чисел (2 example): " + count);
    }
}
package thirdweek;

import java.util.Scanner;

/*
Даны два числа m и n.
Найти произведение чисел в диапазоне между m и n включительно.
(m < 14 и n < 14), m < n

4 12 -> 79833600
3 7 -> 2520
 */
public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int res = 1;

        if ((m < 14 && n < 14) && (m < n)) {
            for (int i = m; i <= n; ++i) {
                res *= i; //res = res * i

            }
            System.out.println("Итоговый результат: " + res);
        } else {
            System.out.println("Некорректные числа");
        }
    }
}
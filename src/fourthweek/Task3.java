package fourthweek;
import java.util.Scanner;
public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
//        boolean result = false;
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
        System.out.println(checkIfArraySortedDesc(arr));

//        for (int i = 0; i < n - 1; i++) {
//            if (arr[i] <= arr[i + 1]) {
//                result = false;
//                break;
//            }
//        }

//        boolean result = checkIfArraySortedDesc(arr);
//        System.out.println(result);
    }

    static boolean checkIfArraySortedDesc(int[] inputArray, int n) {
        for (int i = 0; i < n - 1; i++) {
            if (inputArray[i] <= inputArray[i + 1]) {
                return false;
            }
        }
        return true;
    }

    static boolean checkIfArraySortedDesc(int[] inputArray) {
        return checkIfArraySortedDesc(inputArray, inputArray.length);
    }
}

package fourthweek;

import java.util.Arrays;
import java.util.Scanner;

/*
   На вход подается два отсортированных массива.
   Нужно создать отсортированный третий массив,
   состоящий из элементов первых двух.

   Входные данные:
   5
   1 2 3 4 7

   2
   1 6

   Выходные данные:
   1 1 2 3 4 6 7
    */
public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] firstArray = new int[n];
        for (int i = 0; i < n; i++) {
            firstArray[i] = scanner.nextInt();
        }

        int k = scanner.nextInt();
        int[] secondArray = new int[k];
        for (int i = 0; i < k; i++) {
            secondArray[i] = scanner.nextInt();
        }
        long start = System.currentTimeMillis();
        mergeTwoArraysWithLoopAndPrint(firstArray, secondArray);
        System.out.println(System.currentTimeMillis() - start);

        long start1 = System.currentTimeMillis();
        mergeTwoArraysWithSystemArrayCopyAndPrint(firstArray, secondArray);
        System.out.println(System.currentTimeMillis() - start1);

        long start2 = System.currentTimeMillis();
        mergeTwoArraysAndPrint(firstArray, secondArray);
        System.out.println(System.currentTimeMillis() - start2);
    }

    /**
     * Метод делает слияние двух отсортированных массивов в третий результирующий
     *
     * @param firstArray  первый отсортированный массив
     * @param secondArray второй отсортированный массив
     */
    static void mergeTwoArraysWithLoopAndPrint(int[] firstArray, int[] secondArray) {
        int[] mergedArray = new int[firstArray.length + secondArray.length];

        int pos = 0;
        //копируем элементы первого массива в наш результирующий
        for (int element : firstArray) {
            mergedArray[pos] = element;
            pos++;
        }


        //копируем элементы второго массива в наш результирующий
        for (int element : secondArray) {
            mergedArray[pos] = element;
            pos++;
        }

        //Сортировка результирующего массива
        Arrays.sort(mergedArray);

        //вывод на экран результата
        System.out.println(Arrays.toString(mergedArray));
//        for (int element : mergedArray) {
//            System.out.print(element);
//        }
    }

    static void mergeTwoArraysWithSystemArrayCopyAndPrint(int[] firstArray, int[] secondArray) {
        int[] mergedArray = new int[firstArray.length + secondArray.length];
        System.arraycopy(firstArray, 0, mergedArray, 0, firstArray.length);
        System.arraycopy(secondArray, 0, mergedArray, firstArray.length, secondArray.length);
        Arrays.sort(mergedArray);
        System.out.println(Arrays.toString(mergedArray));
    }

    static void mergeTwoArraysAndPrint(int[] firstArray, int[] secondArray) {
        int[] mergedArray = new int[firstArray.length + secondArray.length];
        int i = 0, j = 0, k = 0;

        //обход двух массивов
        while (i < firstArray.length && j < secondArray.length) {
            if (firstArray[i] < secondArray[j]) {
                mergedArray[k++] = firstArray[i++];
            } else {
                mergedArray[k++] = secondArray[j++];
            }
        }

        //сохраняем оставшиеся значения первого массива
        while (i < firstArray.length) {
            mergedArray[k++] = firstArray[i++];
        }

        //сохраняем оставшиеся значения второго массива
        while (j < secondArray.length) {
            mergedArray[k++] = secondArray[j++];
        }

        System.out.println(Arrays.toString(mergedArray));
    }
}


package fourthweek;
import java.util.Scanner;
public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n];
        boolean flag = false;
        //Заполняем массив пользовательскими данными
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }

        for (int i = 0; i < n; i++) {
            if (arr[i] % 2 == 0) {
                System.out.println("Элемент массива четный: " + arr[i]);
                flag = true;
            }
        }
        if (!flag) {
            System.out.println("Четных элементов нет: " + -1);
        }
    }
}

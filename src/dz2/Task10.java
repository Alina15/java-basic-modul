package dz2;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double x = scanner.nextDouble();
        if ((Math.log(Math.pow(Math.E,x)))  == x) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }

    }
}

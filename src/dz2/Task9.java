package dz2;

import java.util.Locale;
import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        int y;
        y =  (int)(Math.sin(Math.pow(x,2)) + Math.cos(Math.pow(x,2)) - 1);
        if (y == 0) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }

    }
}

package dz2;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        if (x < 500) {
            System.out.println("beginner");
        } else if (x < 1500 && x >= 500) {
            System.out.println("pre-intermediate");
        }else if (x < 2500 && x >= 1500) {
            System.out.println("intermediate");
        }else if (x < 3500 && x >= 2500) {
            System.out.println("upper-intermediate");
        }else {
            System.out.println("fluent");
        }

    }
}

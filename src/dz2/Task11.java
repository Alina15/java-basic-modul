package dz2;

import java.util.Scanner;

public class Task11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        if (a < b + c && b < a + c && c < a + b) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }

    }
}

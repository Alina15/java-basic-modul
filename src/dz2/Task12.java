package dz2;

import java.util.Scanner;

public class Task12 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.next();
        int len = str.length();
        int j=0;
        char ch = str.charAt(0);
        boolean uppercase = false, lowercase = false, numbers = false, signs = false;
        if (len > 7) {

            for (int i = 0; i < len; i++){
                ch = str.charAt(i);
                if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z')){
                    if (Character.isLowerCase(ch)){
                    lowercase = true;
                }
                if (Character.isUpperCase(ch)){
                    uppercase = true;
                }}else {
                    if (ch == '_' || ch == '*'|| ch == '-'){
                        signs = true;
                    }
                if (Character.isDigit(ch)){
                    numbers = true;
                }}
            }
            if (uppercase && signs && lowercase && numbers){
                System.out.println("пароль надежный");
            } else {
                System.out.println("пароль не прошел проверку");
            }
        } else {
            System.out.println("пароль не прошел проверку");
        }
    }
}

package homework11;

import java.util.Set;
import java.util.stream.Collectors;

public class Task6 {
    public static void main(String[] args) {
        Set<Set<Integer>> numbers = Set.of(Set.of(0, 1, 2));
        Set<Integer>numbersFin = numbers.stream()
                .flatMap(Set::stream)
                .collect(Collectors.toSet());
        System.out.println(numbersFin);

    }
}

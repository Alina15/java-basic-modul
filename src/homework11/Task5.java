package homework11;

import java.util.List;
import java.util.stream.Collectors;

public class Task5 {
    public static void main(String[] args) {
        List<String> str = List.of("abc","def","qqq");

        String sum = str.stream()
                .map(String::toUpperCase)
                .collect(Collectors.joining(", "));

        System.out.println(sum);
    }
}

package homework11;


import java.util.ArrayList;

public class Task1 {
    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<Integer>(100);
        for (int i = 1; i <= 100; i++)
        {
            numbers.add(i);
        }
        Integer sum = numbers.stream()
                .reduce(0, Integer::sum);
        System.out.println(sum);

    }
}

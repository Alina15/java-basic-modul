package homework11;

import java.util.ArrayList;
import java.util.List;

public class Task2 {
    public static void main(String[] args) {
        List<Integer> numbers = List.of(1, 2, 3, 4, 5);

        Integer sum = numbers.stream()
                .reduce(1, (x,y) -> x*y);
        System.out.println(sum);

    }

}

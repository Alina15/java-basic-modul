package homework11;

import java.util.Collections;
import java.util.List;

public class Task4 {
    public static void main(String[] args) {
        List<Double> numbers = List.of(1.2,6.7,9.0,3.5,4.4);

        numbers.stream()
                        .sorted(Collections.reverseOrder())
                        .forEach(System.out::println);

    }
}

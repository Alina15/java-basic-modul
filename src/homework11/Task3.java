package homework11;

import java.util.List;

public class Task3 {
    public static void main(String[] args) {
        List<String> str = List.of("abc","", "","def","qqq");

        long sum = str.stream()
                .filter(s -> !s.isEmpty())
                .count();

        System.out.println(sum);
    }
}

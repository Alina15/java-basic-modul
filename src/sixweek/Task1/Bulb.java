package sixweek.Task1;

public class Bulb {
    private boolean toggle;

    public Bulb() {
        this.toggle = false;
    }

    public Bulb(boolean toggle) {
        this.toggle = toggle;
    }

    public void turnOn() {
        this.toggle = true;
    }

    public void turnOff() {
        this.toggle = false;
    }

    public boolean isShining() {
        return this.toggle;
    }
}

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Test {
    public static void main(String[] args) {
        List<String> animalsList = List.of("слон", "слон", "кот", "собака", "мышь", "мышь", "мышь");

        Map<String, Integer> animalsCountMap = countAnimals(animalsList);
        System.out.println(animalsCountMap);

    }

    public static Map<String, Integer> countAnimals(List<String> animals) {
        Map<String, Integer> animalsCountMap = new HashMap<>();

        for (String animal : animals) {
            animalsCountMap.put(animal, animalsCountMap.getOrDefault(animal, 0) + 1);
        }


        return animalsCountMap;
    }
}

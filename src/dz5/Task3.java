package dz5;

import java.util.Scanner;

public class Task3 {
    public static final int[] row = { 2, 1, -1, -2, -2, -1, 1, 2, 2 };
    public static final int[] col = { 1, 2, 2, 1, -1, -2, -2, -1, 1 };
    private static boolean isValid(int x, int y, int n)
    {
        return x >= 0 && y >= 0 && x < n && y < n;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int a = scanner.nextInt();
        int b = scanner.nextInt();


        char[][] array = new char[n][n];


        for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {

                    array[j][i] = '0';
            }
        }
        array[b][a] = 'K';

            for (int k = 0; k < 8; k++)
            {

                int newX = b + row[k];
                int newY = a + col[k];

                if (isValid(newX, newY, n)) {
                    array[newX][newY] = 'X';
                }

        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }
}

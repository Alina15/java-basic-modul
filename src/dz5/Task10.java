package dz5;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int s = scanner.nextInt();
        reversDigits(s);
    }

    public static void reversDigits(int number) {
        System.out.print(number % 10 + " ") ;
        if (number >= 10) {
            reversDigits(number / 10);
        }
    }
}

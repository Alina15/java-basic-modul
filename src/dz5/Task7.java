package dz5;

import java.util.Arrays;
import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String[] nameP = new String[n];
        String[] nameD = new String[n];
        int[][] array = new int[n][3];
        for (int i = 0; i < n; i++) {
            nameP[i] = scanner.next();
        }
        for (int i = 0; i < n; i++) {
            nameD[i] = scanner.next();
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 3; j++) {
                array[i][j] = scanner.nextInt();
            }
        }
//        for (int i = 0; i < n; i++) {
//            for (int j = 0; j < 3; j++) {
//                System.out.print(array[i][j] + " ");
//            }
//            System.out.println();
//        }
        double[] value = new double[n];
        for (int i = 0; i < n; i++) {
            value[i]= (Double)Math.floor((double) (array[i][0] + array[i][1] + array[i][2])/3.0*10)/10.0;

        }
//        System.out.println(Arrays.toString(value));
        int size = value.length;
        double[] temp =  value.clone();
        Arrays.sort(temp);
        for (int i = 0; i < 3; i++) {
            System.out.println(nameP[getIndex(value,  temp[size - (i + 1)])] +": " + nameD[getIndex(value,  temp[size - (i + 1)])] +
                    ", " + value[getIndex(value,  temp[size - (i + 1)])]);
        }

    }

    static int getIndex(double[] value,double v) {
        int temp = 0;
        for (int i = 0; i < value.length; i++) {
            if (value[i] == v) {
                temp = i;
                break;
            }
        }
        return temp;
    }
}

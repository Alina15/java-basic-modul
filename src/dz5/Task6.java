package dz5;

import java.util.Arrays;
import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] col = new int[4];
        int m = 4;
        int n = 7;
        boolean sum = true;
        for (int i = 0; i < 4; i++) {
            col[i] = scanner.nextInt();

        }
        int[][] array = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                array[i][j] = scanner.nextInt();
            }
        }
        int[] result = new int[m];
        int o=0;
        //считаем сумму
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                result[o] += array[j][i];
            }
            o++;
        }

        for (int i = 0; i < 4; i++){

            if (result[i] >= col[i]){
                System.out.println("Нужно есть поменьше");
                sum=false;
                break;
            }
        }
        if(sum)
        System.out.println("Отлично");
    }


}

package dz5;

import java.util.Arrays;
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();

        //заполняем наш массив
        int[][] array = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                array[i][j] = scanner.nextInt();
            }
        }
        //вывод на экран для наглядности


        int[] result = new int[n];
        //считаем сумму
        for (int i = 0; i < n; i++) {
            result[i]=array[i][0];
            for (int j = 0; j < m; j++) {
                if(result[i]>array[i][j])
                    result[i]=array[i][j];
            }
        }
        for (int i = 0; i < n; i++) {
            System.out.println(result[i]); }

    }
}

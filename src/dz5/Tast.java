package dz5;


   import java.util.Arrays;
   import java.util.Random;

public class Tast
    {
        public static void main(String[] args) {
            Random r = new Random();

            int k = 4;                                              // number of rows in an array
            int m = 3;                                              // number of columns in an array

            int[][] fullArray = new int[k][m];                      // initial array

            //filling an array

            for (int i = 0; i < fullArray.length; i++){
                for (int j = 0; j < fullArray[i].length; j++){
                    fullArray[i][j] = r.nextInt(10) + 1;
                }
            }

            //printing an array

            for (int i = 0; i < fullArray.length; i++){
                for (int j = 0; j < fullArray[i].length; j++){
                    System.out.print(fullArray[i][j] + "\t");
                }
                System.out.println();
            }

            //make a copy of an initial array

            int[][] newArray = new int[fullArray.length][];
            for (int i = 0; i < fullArray.length; i++){
                int[] aFullArray = fullArray[i];
                int aLength = aFullArray.length;
                newArray[i] = new int[aLength];
                System.arraycopy(aFullArray, 0, newArray[i], 0, aLength);
            }

            int a = r.nextInt(k);                                   // random row to delete
            int b = r.nextInt(m);                                   // random column to delete

            System.out.println("**********************");

            //printing new array

            for (int i = 0; i < newArray.length; i++){
                for (int j = 0; j < newArray[i].length; j++){
                    System.out.print(newArray[i][j] + "\t");
                }
                System.out.println();
            }

            System.out.println("Row to delete: " + a);
            System.out.println("Column to delete: " + b);

            System.out.println("****************");

            for (int i = 0; i < newArray.length; i++){
                for (int j = 0; j < newArray[i].length; j++){
                    if(i == a){
                        if (i != k - 1)
                        {
                            i = i+1;
                        } else {
                            continue;
                        }
                        newArray[a][b] = newArray[i][j];
                    }
                    if (j == b){
                        if (j != m - 1){
                            j = j + 1;
                        } else {
                            continue;
                        }
                        newArray[a][b] = newArray[i][j];
                    }
                    System.out.print(newArray[i][j] + "\t");
                }
                System.out.println();
            }
        }
    }

package dz5;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int sum=0;
        System.out.println(sumRecursively(a, sum));
    }

    public static int sumRecursively(int a, int sum) {
        sum+= (a % 10);
        if (a == 0) {
            return sum;
        }
        return sumRecursively(a/10,sum);
    }
}

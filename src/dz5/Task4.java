package dz5;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[][] array = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                array[i][j] = scanner.nextInt();
            }
        }
        int p = scanner.nextInt();
        int[][] res = new int[n-1][n-1];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (array[i][j] == p) {
                    fillWithZero(array, n, i, j, res);
                }
            }
        }

        for (int i = 0; i < n-1; i++) {
            for (int j = 0; j < n-1; j++) {
                System.out.print(res[i][j] + " ");
            }
            System.out.println();
        }
    }

    private static void fillWithZero(int[][] array, int k, int a, int b,int[][] res) {
        int w = 0;
        for (int i = 0; i < k; ++i) {
            if (i == a)
                continue;


            int q = 0;
            for (int j = 0; j < k; ++j) {
                if (j == b)
                    continue;

                res[w][q] = array[i][j];
                ++q;
            }

            ++w;
        }


    }
    }


package homework6.Task7;
/*
Реализовать класс TriangleChecker, статический метод которого принимает три длины сторон треугольника и возвращает true,
если возможно составить из них треугольник, иначе false. Входные длины сторон треугольника — числа типа double.
Придумать и написать в методе main несколько тестов для проверки работоспособности класса
(минимум один тест на результат true и один на результат false)

 */
public class Main {
    public static void main(String[] args) {
        TriangleChecker triangleChecker1 = new TriangleChecker(1,2,3);
        System.out.println(triangleChecker1.MakingTriangle());

        TriangleChecker triangleChecker2 = new TriangleChecker(3,4,5);
        System.out.println(triangleChecker2.MakingTriangle());
    }
}

package homework6.Task7;

public class TriangleChecker {
    private final double a;
    private final double b;
    private final double c;

    public TriangleChecker(double a, double b, double c){
        this.a = a;
        this.b = b;
        this.c = c;

    }
    public boolean MakingTriangle(){
        return a < (b + c) && b < (a + c) && c < (a + b);
    }
}

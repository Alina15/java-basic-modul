package homework6.Task3;
import homework6.Task2.Student;

import java.util.Arrays;
import java.util.Comparator;

public class StudentService {
    public static Object bestStudent(Student[] students) {

        Student bestStudent = students[0];
        double maxAverageGrade = bestStudent.calculateAverageGrade();

        for (int i = 1; i < students.length; i++) {
            double currentAverageGrade = students[i].calculateAverageGrade();
            if (currentAverageGrade > maxAverageGrade) {
                maxAverageGrade = currentAverageGrade;
                bestStudent = students[i];
            }
        }

        return bestStudent;
    }
    public static void sortBySurname(Student[] students) {
        Arrays.sort(students, Comparator.comparing(Student::getSurname));
    }
}

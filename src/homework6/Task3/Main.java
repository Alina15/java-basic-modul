package homework6.Task3;

import homework6.Task2.Student;
/*
Необходимо реализовать класс StudentService.
У класса должны быть реализованы следующие публичные методы:
bestStudent() — принимает массив студентов (класс Student из предыдущего задания), возвращает лучшего студента (т.е. который имеет самый высокий средний балл). Если таких несколько — вывести любого.
sortBySurname() — принимает массив студентов (класс Student из предыдущего задания) и сортирует его по фамилии.
 */
public class Main {
        public static void main(String[] args) {

            Student[] students = new Student[3];
            students[0] = new Student("Петр", "Петров", new int[]{80, 78, 85, 92, 89});
            students[1] = new Student("Иван", "Иванов", new int[]{90, 85, 92, 88, 95});
            students[2] = new Student("Мария", "Сидорова", new int[]{95, 92, 88, 96, 94});

            Student best = (Student) StudentService.bestStudent(students);
            System.out.println("Лучший студент: " + best.getName() + " " + best.getSurname());

            StudentService.sortBySurname(students);

            System.out.println("Студенты после сортировки по фамилии:");
            for (Student student : students) {
                System.out.println(student.getSurname() + " " + student.getName());
            }
        }
    }

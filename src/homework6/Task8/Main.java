package homework6.Task8;

/*
Реализовать класс “банкомат” Atm.
Класс должен:
Содержать конструктор, позволяющий задать курс валют перевода долларов в рубли и курс валют перевода рублей в доллары (можно выбрать и задать любые положительные значения)
Содержать два публичных метода, которые позволяют переводить переданную сумму рублей в доллары и долларов в рубли
Хранить приватную переменную счетчик — количество созданных инстансов класса Atm и публичный метод, возвращающий этот счетчик (подсказка: реализуется через static)

 */

public class Main {
    public static void main(String[] args) {
        Atm atm1 = new Atm(87.87, 1.98);
        Atm atm2 = new Atm(101,0.01138);

        double rubles1 = 100;
        double dollars1 = atm1.rublesToDollars(rubles1);
        System.out.println(rubles1 + " рублей = " + dollars1 + " долларов");

        double dollars2 = 50;
        double rubles2 = atm2.dollarsToRubles(dollars2);
        System.out.println(dollars2 + " долларов = " + rubles2 + " рублей");

        System.out.println("Количество созданных Atm: " + Atm.getInstance());
    }

}

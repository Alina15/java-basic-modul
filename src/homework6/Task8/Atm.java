package homework6.Task8;


public class Atm {
    private static int instance = 0;

    private final double rubToDol;
    private final double dolToRub;

    public Atm(double rubToDol, double dolToRub){
        this.rubToDol = rubToDol;
        this.dolToRub = dolToRub;
        instance++;
    }


    public double rublesToDollars(double rubles){
        if (rubles < 0){
            throw new IllegalArgumentException("Сумма рублей должна быть неотрицательной");
        }
        return rubles / rubToDol;
    }

    public double dollarsToRubles(double dollars){
        if (dollars< 0){
            throw new IllegalArgumentException("Сумма долларов должна быть неотрицательной");
        }
        return dollars * dolToRub;
    }

    public static int getInstance(){
        return instance;
    }
}

package homework6.Task1;


import java.util.Random;

public class Cat {
    public static void status(){
        Random r = new Random();
        Cat cat = new Cat();
        int num = r.nextInt(3);
        switch(num) {
            case 0:
                cat.sleep();
                break;
            case 1:
                cat.meow();
                break;
            case 2:
                cat.eat();
                break;

        }
    }
    private void sleep(){
        System.out.println("Sleep");
    }
    private void meow(){
        System.out.println("Meow");
    }
    private void eat(){
        System.out.println("Eat");
    }
    private Cat() {

    }
}

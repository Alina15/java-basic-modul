package homework6.Task4;

public class TimeUnit {
    private int hours;
    private int minutes;
    private int seconds;

    public TimeUnit(int hours, int minutes, int seconds) {
        setHours(hours);
        setMinutes(minutes);
        setSeconds(seconds);
    }

    public TimeUnit(int hours, int minutes) {
        setHours(hours);
        setMinutes(minutes);
        this.seconds=0;
    }

    public TimeUnit(int hours) {
        setHours(hours);
        this.minutes=0;
        this.seconds=0;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        if (hours >= 0 && hours <= 23) {
            this.hours = hours;
        } else {
            throw new IllegalArgumentException("Недопустимое значение для часов. Допустимый диапазон: 0-23");
        }
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        if (minutes >= 0 && minutes <= 59) {
            this.minutes = minutes;
        } else {
            throw new IllegalArgumentException("Недопустимое значение для минут. Допустимый диапазон: 0-59");
        }
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        if (seconds >= 0 && seconds <= 59) {
            this.seconds = seconds;
        } else {
            throw new IllegalArgumentException("Недопустимое значение для секунд. Допустимый диапазон: 0-59");
        }
    }

    public void displayTime() {
        System.out.printf("%02d:%02d:%02d%n", hours, minutes, seconds);
    }

    public void display12HourFormat() {
        String amPm = hours < 12 ? "AM" : "PM";
        int displayHours = hours % 12 == 0 ? 12 : hours % 12;
        System.out.printf("%02d:%02d:%02d %s%n", displayHours, minutes, seconds, amPm);
    }

    public void addTime(int addHours, int addMinutes, int addSeconds) {
        int newSeconds = (this.seconds + addSeconds) % 60;
        int carryMinutes = (this.seconds + addSeconds) / 60;
        int newMinutes = (this.minutes + addMinutes + carryMinutes) % 60;
        int carryHours = (this.minutes + addMinutes + carryMinutes) / 60;
        int newHours = (this.hours + addHours + carryHours) % 24;

        setHours(newHours);
        setMinutes(newMinutes);
        setSeconds(newSeconds);
    }
}

package homework6.Task4;
/*
Необходимо реализовать класс TimeUnit с функционалом, описанным ниже (необходимые поля продумать самостоятельно). Обязательно должны быть реализованы валидации на входные параметры.
Конструкторы:
Возможность создать TimeUnit, задав часы, минуты и секунды.
Возможность создать TimeUnit, задав часы и минуты. Секунды тогда должны проставиться нулевыми.
Возможность создать TimeUnit, задав часы. Минуты и секунды тогда должны проставиться нулевыми.
Публичные методы:
Вывести на экран установленное в классе время в формате hh:mm:ss
Вывести на экран установленное в классе время в 12-часовом формате (используя hh:mm:ss am/pm)
Метод, который прибавляет переданное время к установленному в TimeUnit (на вход передаются только часы, минуты и секунды).

 */
public class Main {
    public static void main(String[] args) {
        TimeUnit time1 = new TimeUnit(10, 30, 45);
        time1.displayTime();
        time1.display12HourFormat();

        TimeUnit time2 = new TimeUnit(4, 45);
        time2.displayTime();
        time2.display12HourFormat();

        TimeUnit time3 = new TimeUnit(18);
        time3.displayTime();
        time3.display12HourFormat();

        time1.addTime(3, 20, 15);
        System.out.println("После добавления времени:");
        time1.displayTime();
    }
}

package homework6.Task2;

import java.util.Arrays;

/*
Необходимо реализовать класс Student.
У класса должны быть следующие приватные поля:
String name — имя студента
String surname — фамилия студента
int[] grades — последние 10 оценок студента. Их может быть меньше, но не может быть больше 10.
И следующие публичные методы:
геттер/сеттер для name
геттер/сеттер для surname
геттер/сеттер для grades
метод, добавляющий новую оценку в grades. Самая первая оценка должна быть удалена, новая должна сохраниться в конце массива (т.е. массив должен сдвинуться на 1 влево).
метод, возвращающий средний балл студента (рассчитывается как среднее арифметическое от всех оценок в массиве grades)

 */
public class Main {
        public static void main(String[] args) {
            int[] grades = {85, 90, 92, 88, 95};
            Student student = new Student("Иван", "Иванов", grades);

            System.out.println("Имя: " + student.getName());
            System.out.println("Фамилия: " + student.getSurname());
            System.out.println("Оценки: " + Arrays.toString(student.getGrades()));
            System.out.println("Средний балл: " + student.calculateAverageGrade());

            student.addGrade(97);
            System.out.println("Обновленные оценки: " + Arrays.toString(student.getGrades()));
            System.out.println("Новый средний балл: " + student.calculateAverageGrade());
        }
    }


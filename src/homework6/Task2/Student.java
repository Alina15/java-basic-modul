package homework6.Task2;


public class Student {

        private String name;
        private String surname;
        private final int[] grades;

        public Student(String name, String surname, int[] grades) {
            this.name = name;
            this.surname = surname;
            this.grades = new int[10];
            int length = Math.min(grades.length, 10);
            System.arraycopy(grades, 0, this.grades, 10 - length, length);
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSurname() {
            return surname;
        }

        public void setSurname(String surname) {
            this.surname = surname;
        }

        public int[] getGrades() {
            return grades;
        }

        public void setGrades(int[] grades) {
            int length = Math.min(grades.length, 10);
            System.arraycopy(grades, 0, this.grades, 10 - length, length);
        }

        public void addGrade(int newGrade) {
            System.arraycopy(grades, 1, grades, 0, 9);
            grades[9] = newGrade;
        }

        public double calculateAverageGrade() {
            int sum = 0;
            for (int grade : grades) {
                sum += grade;
            }
            return (double) sum / grades.length;
        }
    }




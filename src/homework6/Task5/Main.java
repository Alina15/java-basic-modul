package homework6.Task5;
/*
Необходимо реализовать класс DayOfWeek для хранения порядкового номера дня недели (byte) и названия дня недели (String).
Затем в отдельном классе в методе main создать массив объектов DayOfWeek длины 7. Заполнить его соответствующими значениями (от 1 Monday до 7 Sunday) и вывести значения массива объектов DayOfWeek на экран.
Пример вывода:
1 Monday
2 Tuesday
…
7 Sunday

 */
public class Main {
    public static void main(String[] args) {
        DayOfWeek[] daysOfWeek = new DayOfWeek[7];

        daysOfWeek[0] = new DayOfWeek((byte) 1, "Monday");
        daysOfWeek[1] = new DayOfWeek((byte) 2, "Tuesday");
        daysOfWeek[2] = new DayOfWeek((byte) 3, "Wednesday");
        daysOfWeek[3] = new DayOfWeek((byte) 4, "Thursday");
        daysOfWeek[4] = new DayOfWeek((byte) 5, "Friday");
        daysOfWeek[5] = new DayOfWeek((byte) 6, "Saturday");
        daysOfWeek[6] = new DayOfWeek((byte) 7, "Sunday");

        for (DayOfWeek day : daysOfWeek) {
            System.out.println(day.getDayNumber() + " " + day.getDayName());
        }
    }
}

package homework6.Task5;

public class DayOfWeek {
    private final byte dayNumber;
    private final String dayName;

    public DayOfWeek(byte dayNumber, String dayName) {
        this.dayNumber = dayNumber;
        this.dayName = dayName;
    }

    public byte getDayNumber() {
        return dayNumber;
    }

    public String getDayName() {
        return dayName;
    }
}

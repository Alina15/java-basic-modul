package homework6.Task6;

public class AmazingString {
    private char[] charArray;

    public AmazingString(char[] charArray) {
        this.charArray= charArray;
    }
    public AmazingString(String str) {
        this.charArray = new char[str.length()];
        for (int i = 0; i < str.length(); i++){
            this.charArray[i] = str.charAt(i);
        }
    }

    public char setCharAtIndex(int index) {
        if (index >= 0 && index < charArray.length){
            return charArray[index];
        } else{
            throw new IndexOutOfBoundsException("Недопустимый индекс");
        }
    }

    public int length() {
        return charArray.length;
    }

    public void print() {
        System.out.println(charArray);
    }

    public boolean containsSubstring(char[] substring) {
        int substringLength = substring.length;
        if (substringLength > charArray.length){
            return false;
        }
        for (int i = 0; i <= charArray.length -substringLength; i++){
            boolean found = true;
            for (int j = 0; j < substringLength; j++){
                if (charArray[i + j]!= substring[j]){
                    found = false;
                    break;
                }
            }
            if (found){
                return true;
            }
        }
        return false;
    }

    public boolean containsSubstring(String substring){
        return containsSubstring(substring.toCharArray());
    }

    public void trimLeadingWhitespace() {
        int startWhitespaceCount = 0;
        while (startWhitespaceCount < charArray.length && Character.isWhitespace(charArray[startWhitespaceCount])) {
            startWhitespaceCount++;
        }

        if (startWhitespaceCount > 0) {
            char[] newCharArray = new char[charArray.length - startWhitespaceCount];
            System.arraycopy(charArray, startWhitespaceCount, newCharArray, 0, newCharArray.length);
            charArray = newCharArray;
        }
    }

    public void reverse() {
        int start = 0;
        int end = charArray.length - 1;

        while (start < end){
            char temp = charArray[start];
            charArray[start] = charArray[end];
            charArray[end] = temp;
            start++;
            end--;
        }
    }
}


package homework6.Task6;
/*
Необходимо реализовать класс AmazingString, который хранит внутри себя строку как массив char и предоставляет следующий функционал:
Конструкторы:
Создание AmazingString, принимая на вход массив char
Создание AmazingString, принимая на вход String
Публичные методы (названия методов, входные и выходные параметры продумать самостоятельно). Все методы ниже нужно реализовать “руками”, т.е. не прибегая к переводу массива char в String и без использования стандартных методов класса String.
Вернуть i-ый символ строки
Вернуть длину строки
Вывести строку на экран
Проверить, есть ли переданная подстрока в AmazingString (на вход подается массив char). Вернуть true, если найдена и false иначе
Проверить, есть ли переданная подстрока в AmazingString (на вход подается String). Вернуть true, если найдена и false иначе
Удалить из строки AmazingString ведущие пробельные символы, если они есть
Развернуть строку (первый символ должен стать последним, а последний первым и т.д.)
*/
public class Main {
    public static void main(String[] args) {
        AmazingString amazingString1 = new AmazingString("Hello, World!");
        amazingString1.print();

        System.out.println(amazingString1.setCharAtIndex(7));
        System.out.println(amazingString1.length());

        char[] substring1 = {'W', 'o', 'r', 'l', 'd'};
        System.out.println(amazingString1.containsSubstring(substring1));

        String substring2 = "ello";
        System.out.println(amazingString1.containsSubstring(substring2));

        amazingString1.trimLeadingWhitespace();
        amazingString1.print();

        amazingString1.reverse();
        amazingString1.print();
    }
}

package homework10;

public class CheckAnnotation {
    public static void main(String[] args) {
        printClassAnnotation(Test.class);
    }

    public static void printClassAnnotation(Class<?> cls) {
        if (!cls.isAnnotationPresent(IsLike.class)) {
            return;
        }
        IsLike classDescription = cls.getAnnotation(IsLike.class);
        System.out.println("Понравилось ли: " + classDescription.like());

    }

}

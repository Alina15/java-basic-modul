package homework10.Task4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReflectionGetInterface {
    public static void main(String[] args) {
        List<Class<?>> res = getAllInterfaces(D.class);
        for (Class<?> cls : res) {
            System.out.println(cls.getName());
        }

    }
    public static List<Class<?>> getAllInterfaces(Class<?> cls) {
        List<Class<?>> interfacesFound = new ArrayList<>();
        getAllInterfaces(cls, interfacesFound);
        return interfacesFound;
    }
    private static void getAllInterfaces(Class<?> cls, List<Class<?>> interfacesFound) {
        while (cls != null) {
            Class<?>[] interfaces = cls.getInterfaces();
            for (Class<?> anInterface : interfaces) {
                if (!interfacesFound.contains(anInterface)) {
                    interfacesFound.add(anInterface);
                    getAllInterfaces(anInterface, interfacesFound);
                }
            }
            cls = cls.getSuperclass();
        }
    }
}

package homework10.Task3;

import java.lang.reflect.Method;

public class ReflectionExample {
    public static void main(String[] args) {
        APrinter aPrinter = new APrinter();

        invokePrintMethod(aPrinter, 42);
    }

    public static void invokePrintMethod(APrinter aPrinter, int argument) {
        try {
            Class<?> clazz = APrinter.class;
            Method printMethod = clazz.getMethod("print", int.class);
            printMethod.invoke(aPrinter, argument);

        } catch (NoSuchMethodException e) {
            System.out.println("Метод print с параметром int не найден.");
        } catch (Exception e) {
            System.out.println("Произошла ошибка при вызове метода print:");
            System.out.println(e.getMessage());
        }
    }
}

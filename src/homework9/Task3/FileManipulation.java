package homework9.Task3;

import java.io.*;
import java.nio.file.Path;
import java.util.Scanner;

public class FileManipulation {
    public static void processFile() {
        String strInputFileName = "src\\homework9\\Task3\\input";
        String strOutputFileName = "src\\homework9\\Task3\\output";
        Path inputFileName = Path.of(strInputFileName).toAbsolutePath();
        Path outputFileName = Path.of(strOutputFileName).toAbsolutePath();
        try {
            Scanner scanner = new Scanner(new File(String.valueOf(inputFileName)));
            Writer writer = new FileWriter(String.valueOf(outputFileName));
            String line;
            while (scanner.hasNextLine()) {
                line = scanner.nextLine();
                String convertedLine = convertToUpperCase(line);
                convertedLine+="\n";
                writer.write(convertedLine);
            }
            writer.close();
            scanner.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private static String convertToUpperCase(String input) {
        StringBuilder result = new StringBuilder();
        for (char c : input.toCharArray()) {
            result.append(Character.isLowerCase(c) ? Character.toUpperCase(c) : c);
        }
        return result.toString();
    }
}

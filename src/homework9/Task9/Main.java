package homework9.Task9;

import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Set<Integer> set1 = new HashSet<>();
        set1.add(1);
        set1.add(2);
        set1.add(3);

        Set<Integer> set2 = new HashSet<>();
        set2.add(0);
        set2.add(1);
        set2.add(2);
        set2.add(4);

        PowerfulSet powerfulSet = new PowerfulSet();

        Set<Integer> intersectionResult = powerfulSet.intersection(set1, set2);
        System.out.println("Пересечение: " + intersectionResult);

        Set<Integer> unionResult = powerfulSet.union(set1, set2);
        System.out.println("Объединение: " + unionResult);

        Set<Integer> relativeComplementResult = powerfulSet.relativeComplement(set1, set2);
        System.out.println("Элементы первого набора без тех, которые находятся также и во втором наборе.: " + relativeComplementResult);
    }
}

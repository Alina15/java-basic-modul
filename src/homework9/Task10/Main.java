package homework9.Task10;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {

        List<Document> documents = new ArrayList<>();
        documents.add(new Document(1, "Document1", 10));
        documents.add(new Document(2, "Document2", 15));
        documents.add(new Document(3, "Document3", 8));
        Map<Integer, Document> organizedDocuments = DocumentOrganizer.organizeDocuments(documents);


    }
}

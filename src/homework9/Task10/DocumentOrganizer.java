package homework9.Task10;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DocumentOrganizer {
    public static Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map<Integer, Document> organizedMap = new HashMap<>();

        for (Document document : documents) {
            organizedMap.put(document.id, document);
        }

        return organizedMap;
    }
}

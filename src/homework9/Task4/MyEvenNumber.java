package homework9.Task4;

public class MyEvenNumber {
    private final int n;

    public MyEvenNumber(int n) throws OddNumberException {
        if (n % 2 != 0) {
            throw new OddNumberException("Это не четное число");
        }
        this.n = n;
    }

    public int getNumber() {
        return n;
    }
}

package homework9.Task4;

public class Main {
    public static void main(String[] args) {
        try {
            MyEvenNumber evenNumber = new MyEvenNumber(4);
            System.out.println("Четное число: " + evenNumber.getNumber());
        } catch (OddNumberException e) {
            System.out.println(e.getMessage());
        }

        try {
            MyEvenNumber oddNumber = new MyEvenNumber(3);
        } catch (OddNumberException e) {
            System.out.println(e.getMessage());
        }
    }
}

package homework9.Task8;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите первую строку: ");
        String s = scanner.nextLine();

        System.out.print("Введите вторую строку: ");
        String t = scanner.nextLine();

        boolean result = Anagram.isAnagram(s, t);
        System.out.println(result);
    }
}

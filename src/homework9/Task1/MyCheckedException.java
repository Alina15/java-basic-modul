package homework9.Task1;

class MyCheckedException extends Exception {
    public MyCheckedException(String message) {
        super(message);
    }
}

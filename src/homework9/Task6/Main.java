package homework9.Task6;

public class Main {
    public static void main(String[] args) {
        try {
            FormValidator.checkName("Иван");
            FormValidator.checkBirthdate("01.01.1991");
            FormValidator.checkGender("Male");
            FormValidator.checkHeight("175.5");
            System.out.println("Анкета верна!");
        } catch (ValidationException e) {
            System.out.println(e.getMessage());
        }
    }
}

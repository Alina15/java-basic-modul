package homework9.Task6;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class FormValidator {
    public static void checkName(String str) throws ValidationException {
        if (str == null || str.length() < 2 || str.length() > 20 || !Character.isUpperCase(str.charAt(0))) {
            throw new ValidationException("Неверный формат имени");
        }
    }

    public static void checkBirthdate(String str) throws ValidationException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate birthdate = LocalDate.parse(str, formatter);
        LocalDate currentDate = LocalDate.now();

        if (birthdate.isBefore(LocalDate.of(1900, 1, 1)) || birthdate.isAfter(currentDate)) {
            throw new ValidationException("Неверный формат даты рождения");
        }
    }

    public static void checkGender(String str) throws ValidationException {
        try {
            Gender gender = Gender.valueOf(str.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new ValidationException("IНеверный формат гендера");
        }
    }

    public static void checkHeight(String str) throws ValidationException {
        try {
            double height = Double.parseDouble(str);
            if (height <= 0) {
                throw new ValidationException("Неверный рост");
            }
        } catch (NumberFormatException e) {
            throw new ValidationException("Неверный формат роста");
        }
    }
}

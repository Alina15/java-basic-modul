package homework9.Task6;

public class ValidationException extends Exception {
    public ValidationException(String message) {
        super(message);
    }
}
package homework9.Task7;

import java.util.ArrayList;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> integerList = new ArrayList<>();
        integerList.add(1);
        integerList.add(2);
        integerList.add(3);
        integerList.add(1);
        integerList.add(4);

        Set<Integer> uniqueSet = UniqueElements.getUniqueElements(integerList);
        System.out.println(uniqueSet);
    }
}

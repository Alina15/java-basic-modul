package homework9.Task7;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class UniqueElements {
    public static  <T> Set<T> getUniqueElements(ArrayList<T> inputList) {
        return new HashSet<>(inputList);
    }
}
